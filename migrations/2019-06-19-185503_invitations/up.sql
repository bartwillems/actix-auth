-- Your SQL goes here
CREATE TABLE invitations (
    id UUID PRIMARY KEY,
    email TEXT NOT NULL,
    expires_at TIMESTAMP NOT NULL
);