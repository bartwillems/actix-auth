# Actix Auth

This is a small project to test some stuff with actix.

## Build

```bash
cargo build --release
```

## Run

```bash
docker-compose up -d
cargo run
```

## Goals

1. Error logging using sentry
1. small chat/messages/events using websockets
1. Run background tasks & interact with them over an api/websockets