#![allow(dead_code)]

#[macro_use]
extern crate diesel;

#[macro_use]
extern crate serde_derive;

extern crate num_cpus;

use actix::prelude::*;
use actix_web::middleware::Logger;
use actix_web::{web, App, HttpServer};
use diesel::{r2d2::ConnectionManager, PgConnection};
use dotenv::dotenv;

mod errors;
mod models;
mod schema;
mod invitation_handler;

use crate::models::DbExecutor;


fn main() -> std::io::Result<()> {
    dotenv().ok();

    std::env::set_var(
        "RUST_LOG",
        "simple-auth-server=debug,actix_web=info,actix_server=info",
    );

    env_logger::init();

    let sys = actix_rt::System::new("actix-auth");

    let database_url = std::env::var("DATABASE_URL").expect("DATABASE_URL must be set");

    let manager = ConnectionManager::<PgConnection>::new(database_url);
    let pool = r2d2::Pool::builder()
        .build(manager)
        .expect("Unable to create db connection pool.");

    // diesel::migration::Migration::run(conn: &SimpleConnection)

    let address: Addr<DbExecutor> =
        SyncArbiter::start(num_cpus::get(), move || DbExecutor(pool.clone()));

    HttpServer::new(move || {
        App::new()
            .data(address.clone())
            .wrap(Logger::default())
            .service(
                web::scope("/api")
                    .service(web::resource("/auth").route(web::get().to(|| {})))
                    .service(web::resource("/invitation").route(web::get().to(|| {})))
                    .service(
                        web::resource("/register/{invitation_id}").route(web::get().to(|| {})),
                    ),
            )
    })
    .bind("localhost:8080")?
    .start();

    sys.run()
}
